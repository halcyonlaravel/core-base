<?php

return
    [
        'user_timezone' => 'Asia/Manila',

        'formats' => [
            'date' => 'F j, Y',
            'time_12' => 'g:ia',
            'time_24' => 'g:i',
            'datetime_12' => 'm/d/Y h:i A',// 'F jS, Y g:ia',
            'datetime_24' => 'F jS, Y g:i',
        ],

        'api' => [
            'skip_pagination' => env('CORE_BASE_SKIP_PAGINATION', true),
            'limit_pagination' => env('CORE_BASE_LIMIT_PAGINATION', 100),
            'tokens_expire_in_seconds' => env('CORE_BASE_TOKEN_EXPIRE_IN_SECOND', 60 * 60/* 1hr */),
            'refresh_tokens_expire_in_seconds' => env('CORE_BASE_REFRESH_TOKEN_EXPIRE_IN_SECOND', 60 * 60/* 1hr */),
            'social_login' => [
                'facebook' => [
                    'fields' => [
                        'name',
                        'first_name',
                        'last_name',
                        'email',
                    ],
                ],
            ],
        ],

        'listeners' => [
            'repository_action' => env(
                'CORE_BASE_REPOSITORY_ACTION_LISTENER',
                'App\Listeners\RepositoryActionListener'
            ),
        ],

        'statuses' => [
            'default' => [
                'enabled' => 'enabled',
                'disabled' => 'disabled',
            ],
            'default_ui' => [
                'enabled' => '<span class="badge badge-success">:status</span>',
                'disabled' => '<span class="badge badge-danger">:status</span>',
            ],
        ],

        'repositories' => [
            'user' => env('CORE_BASE_USER_REPOSITORY', 'App\Repositories\Auth\UserRepository'),
        ],

        /**
         * @deprecated
         * @todo remove this
         */
        'session_keys' => [
            'flash_success' => 'flash_success',
        ],
    ];
