<?php

return [
    'roles' => [
        'system' => 'system',
        'admin' => 'admin',
        'store_manager' => 'store_manager',
        'cashier' => 'cashier',
        'user' => 'user',
    ],
    'permissions' => [
        'view_backend' => 'view_backend',
        'value_store_settings' => 'setting_edit',
        'value_store_about' => 'about_edit',
        'value_store_contact_us' => 'contact_us_edit',
        'value_store_social' => 'social_edit',
    ],
];
