<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Models;

use Illuminate\Database\Eloquent\Builder;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * HalcyonLaravelBoilerplate\CoreBase\Models\Media
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\HalcyonLaravelBoilerplate\CoreBase\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @method static Builder|Media whereHashedKey($hashed, $column = 'id')
 */
class Media extends \HalcyonLaravelBoilerplate\MediaLibraryUploader\Models\Media implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    /**
     * @return array|\Illuminate\Support\Collection
     */
    public function generatedConversionUrls()
    {
        return $this->getGeneratedConversions()->map(
            fn($status, $generatedConversion) => $this->getFullUrl($generatedConversion)
        );
    }
}
