<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Models\Traits;

use HalcyonLaravelBoilerplate\CoreBase\Models\ActionLinks\ActionLink;
use HalcyonLaravelBoilerplate\CoreBase\Models\ActionLinks\Link;

trait ActionLinkTrait
{
    public function action(string $side, string $type, string $key = 'url'): ?string
    {
        return $this->actionLinks()->getBySideType($side, $type, $key);
    }

    abstract public function actionLinks(): ActionLink;

    /**
     * @param  string  $side
     * @param  array  $types
     *
     * @return Link[]
     */
    public function actions(string $side, array $types)
    {
        return $this->actionLinks()->getBySideTypes($side, $types)->toArray();
    }
}
