<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Models\Traits;

use Illuminate\Support\Str;

trait GenerateResourceConfigTrait
{
    /**
     * @return string
     */
    public static function resourceModelName(): string
    {
        return Str::lower(self::getCurrentBaseClassName());
    }

    /**
     * @return string
     */
    public static function viewBackendPath(): string
    {
        return 'backend.'.self::modelResourceKeyNameName();
    }

    /**
     * @return string
     */
    public static function viewFrontendPath(): string
    {
        return 'frontend.'.self::modelResourceKeyNameName();
    }

    /**
     * @return string
     */
    public static function routeBackendPath(): string
    {
        return 'backend.'.self::modelResourceKeyNameName();
    }

    /**
     * @return string
     */
    public static function routeFrontendPath(): string
    {
        return 'frontend.'.self::modelResourceKeyNameName();
    }

    /**
     * @return string
     */
    private static function getCurrentBaseClassName(): string
    {
        return Str::snake(class_basename(new static()), ' ');
    }

    private static function modelResourceKeyNameName(): string
    {
        return Str::plural(Str::kebab(self::getCurrentBaseClassName()));
    }
}
