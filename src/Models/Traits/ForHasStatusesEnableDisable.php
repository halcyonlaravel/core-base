<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Models\Traits;


use Illuminate\Database\Eloquent\Builder;
use Spatie\ModelStatus\HasStatuses;

/**
 * Trait ForHasStatusesEnableDisable
 *
 * @package HalcyonLaravelBoilerplate\CoreBase\Models\Traits
 *
 * @property-read string $status
 * @method static Builder currentStatus($names)
 * @method static Builder otherCurrentStatus($names)
 * @method static Builder enabled()
 * @method static Builder disable()
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\ModelStatus\Status[] $statuses
 * @property-read int|null $statuses_count
 */
trait ForHasStatusesEnableDisable
{
    use HasStatuses {
        isValidStatus as public isValidStatusOverride;
    }

    public function scopeEnabled(Builder $query): void
    {
        $query->currentStatus(config('core-base.statuses.default.enabled'));
    }

    public function scopeDisabled(Builder $query): void
    {
        $query->currentStatus(config('core-base.statuses.default.disabled'));
    }

    public function isValidStatus(string $name, ?string $reason = null)
    {
        return in_array($name, config('core-base.statuses.default'));
    }

    /**
     * @throws \Spatie\ModelStatus\Exceptions\InvalidStatus
     */
    public function setEnableStatus()
    {
        $this->setStatus(config('core-base.statuses.default.enabled'));
    }

    /**
     * @throws \Spatie\ModelStatus\Exceptions\InvalidStatus
     */
    public function setDisableStatus()
    {
        $this->setStatus(config('core-base.statuses.default.disabled'));
    }

    public function checkEnabledStatus()
    {
        if (!$this->isEnabledStatus()) {
            abort(404);
        }
    }

    public function isEnabledStatus(): bool
    {
        return $this->status() == config('core-base.statuses.default.enabled');
    }

    public function getStatusesEnableDisableHtml()
    {
        if (blank($this->status)) {
            return '--';
        }

        return str_replace(
            ':status',
            trans('core-base::status.default.'.$this->status),
            config('core-base.statuses.default_ui.'.$this->status)
        );
    }
}
