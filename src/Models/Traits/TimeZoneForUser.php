<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Models\Traits;

use Carbon\Carbon;
use HalcyonLaravelBoilerplate\CoreBase\Facades\TimezoneFacade;

trait TimeZoneForUser
{
    /**
     * @param $value
     *
     * @return \Illuminate\Support\Carbon|null
     */
    public function getCreatedAtAttribute($value)
    {
        return $this->formatTimestamp($value);
    }

    /**
     * @param $value
     *
     * @return \Illuminate\Support\Carbon|null
     */
    public function getUpdatedAtAttribute($value)
    {
        return $this->formatTimestamp($value);
    }

    /**
     * @param $value
     *
     * @return \Illuminate\Support\Carbon|null
     */
    public function getDeletedAtAttribute($value)
    {
        return $this->formatTimestamp($value);
    }

    /**
     * @param  string|null  $value
     *
     * @return \Illuminate\Support\Carbon|null
     */
    public function formatTimestamp(?string $value): ?Carbon
    {
        if (is_null($value)) {
            return null;
        }

        return TimezoneFacade::convertToLocal(now()->parse($value));
    }
}
