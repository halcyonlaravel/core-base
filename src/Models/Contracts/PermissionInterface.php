<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Models\Contracts;

interface PermissionInterface
{
    /**
     * @return array
     */
    public static function accessPermissions(): array;
}
