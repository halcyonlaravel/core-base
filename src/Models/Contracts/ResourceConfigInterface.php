<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Models\Contracts;

interface ResourceConfigInterface
{
    /**
     * @return string
     */
    public static function icon(): string;

    /**
     * @return string
     */
    public static function resourceModelName(): string;

    /**
     * @return string
     */
    public static function viewBackendPath(): string;

    /**
     * @return string
     */
    public static function viewFrontendPath(): string;

    /**
     * @return string
     */
    public static function routeBackendPath(): string;

    /**
     * @return string
     */
    public static function routeFrontendPath(): string;

}
