<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Models\ActionLinks;

use HalcyonLaravelBoilerplate\CoreBase\Exceptions\ActionLinkException;
use HalcyonLaravelBoilerplate\CoreBase\Models\Contracts\PermissionInterface;
use HalcyonLaravelBoilerplate\CoreBase\Models\Contracts\ResourceConfigInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;

class ActionLink
{
    private array $links;

    private ResourceConfigInterface $model;

    /**
     * @return static
     */
    public static function create(): self
    {
        return (new static())->reset();
    }

    /**
     * @return $this
     */
    public function reset(): self
    {
        $this->links = [];
        return $this;
    }

    /**
     * @param  string  $side
     * @param  \HalcyonLaravelBoilerplate\CoreBase\Models\ActionLinks\Link  $link
     *
     * @return $this
     */
    public function add(string $side, Link $link): self
    {
        $this->links[$side][] = $link;

        return $this;
    }

    public function model(ResourceConfigInterface $model)
    {
        $this->model = $model;

        return $this;
    }

    public function addResource()
    {
        return $this->generate(['show', 'edit', 'destroy']);
    }

    public function addSoftDelete()
    {
        return $this->generate(['restore', 'purge']);
    }

    public function getBySideTypes(string $side, array $types): Collection
    {
        return collect($this->getBySide($side))
            ->filter(
                fn(Link $link) => in_array($link->type, $types) && $this->can($link)
            )->map(
                function (Link $link) use ($types) {
                    $link->order = array_search($link->type, $types);
                    return $link;
                }
            )->sortBy('order');
    }

    /**
     * @param  string  $side
     * @param  string  $type
     * @param  string  $key
     *
     * @return string
     */
    public function getBySideType(string $side, string $type, string $key = 'url'): ?string
    {
        $link = collect($this->getBySide($side))
            ->where('type', $type)->first();

        if (blank($link)) {
            throw ActionLinkException::type($type);
        }

        if (!$this->can($link)) {
            return null;
        }

        return $link->{$key};
    }

    public function generate(array $only, string $side = 'backend')
    {
        foreach ($only as $item) {
            $routeName = $this->model->routeBackendPath().'.'.$item;
            if (Route::has($routeName)) {
                $this->add(
                    $side,
                    Link::make($item, route($routeName, $this->model))
                );
            }
        }

        return $this;
    }

    private function can(Link $link): bool
    {
        if (auth()->guest()) {
            return false;
        }

        /** @var \Illuminate\Foundation\Auth\User|\Illuminate\Contracts\Auth\Authenticatable|null|\Spatie\Permission\Traits\HasRoles $user */
        $user = auth()->user();

        $roleKey = $link->role_key;
        if (filled($roleKey)) {
            return $user->hasRole(config('core-base-permissions.roles.'.$roleKey));
        }

        $permissionKey = $link->permissionKey ?: $link->permission_key;

        if (blank($permissionKey)) {
            return true;
        }

        if (blank($this->model)) {
            abort(500, 'Model required in '.self::class);
        }

        if (!$this->model instanceof PermissionInterface) {
            abort(500, 'Model '.get_class($this->model).' must implement '.PermissionInterface::class);
        }

        $permissionName = $this->model->accessPermissions();

        if (!isset($permissionName[$permissionKey])) {
            return false;
//            abort(
//                500,
//                "Permission key `$permissionKey` not found in ".get_class($this->model)."::accessPermissions()."
//            );
        }

        return $user->can($permissionName[$permissionKey]);
    }

    /**
     * @param  string  $side
     *
     * @return \HalcyonLaravelBoilerplate\CoreBase\Models\ActionLinks\Link[]
     */
    private function getBySide(string $side)
    {
        if (!isset($this->links[$side])) {
            throw ActionLinkException::side($side);
        }

        /** @var \HalcyonLaravelBoilerplate\CoreBase\Models\ActionLinks\Link[] $l */
        $l = $this->links[$side];
        return $l;
    }
}
