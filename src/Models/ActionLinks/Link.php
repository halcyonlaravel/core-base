<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Models\ActionLinks;

use HalcyonLaravelBoilerplate\CoreBase\Exceptions\ActionLinkException;

/**
 * @property string $label
 * @property string $class_btn
 * @property string $class_text
 * @property string $name
 * @property string $icon
 * @property bool $is_more
 * @property string $url_method
 * @property string $toastr_title
 * @property string $toastr_message
 * @property string $swal_title
 * @property string $swal_message
 * @property string $swal_icon
 * @property string $swal_cancel
 * @property string $swal_confirm
 * @property string $permission_key
 * @property string|null $role_key
 */
class Link
{
    private const
        DEFAULT = [
        'show' => [
            'label' => 'Show',
            'class_btn' => 'btn-info',
            'class_text' => 'text-info',
            'name' => 'btn_show',
            'icon' => 'fa fa-eye',
            'is_more' => false,
            'url_method' => 'get',
            'permission_key' => 'show',
            'role_key' => null,
        ],
        'edit' => [
            'label' => 'Edit',
            'class_btn' => 'btn-primary',
            'class_text' => 'text-primary',
            'name' => 'btn_edit',
            'icon' => 'fa fa-pencil',
            'is_more' => false,
            'url_method' => 'get',
            'permission_key' => 'edit',
            'role_key' => null,
        ],
        'destroy' => [
            'label' => 'Delete',
            'class_btn' => 'btn-danger',
            'class_text' => 'text-danger',
            'name' => 'btn_destroy',
            'icon' => 'fa fa-trash',
            'is_more' => false,
            'url_method' => 'delete',
            //
            'toastr_title' => 'Deleted',
            'toastr_message' => 'Data has been deleted',
            'swal_title' => 'Delete',
            'swal_message' => 'Are you sure you want to delete?',
            'swal_icon' => 'error',
            'swal_cancel' => 'Cancel',
            'swal_confirm' => 'Delete',
            'permission_key' => 'destroy',
            'role_key' => null,
        ],

        // soft deletes
        'restore' => [
            'label' => 'Restore',
            'class_btn' => 'btn-info',
            'class_text' => 'text-info',
            'name' => 'btn_restore',
            'icon' => 'fa fa-refresh',
            'is_more' => false,
            'url_method' => 'patch',
            //
            'toastr_title' => 'Restored',
            'toastr_message' => 'Data has been restored',
            'swal_title' => 'Restore',
            'swal_message' => 'Are you sure you want to restore?',
            'swal_icon' => 'info',
            'swal_cancel' => 'Cancel',
            'swal_confirm' => 'Restore',
            'permission_key' => 'restore',
            'role_key' => null,
        ],
        'purge' => [
            'label' => 'Force Delete',
            'class_btn' => 'btn-danger',
            'class_text' => 'text-danger',
            'name' => 'btn_purge',
            'icon' => 'fa fa-trash',
            'is_more' => false,
            'url_method' => 'delete',
            //
            'toastr_title' => 'Permanent Deleted',
            'toastr_message' => 'Data has been permanent deleted',
            'swal_title' => 'Permanent Delete',
            'swal_message' => 'Are you sure you want to permanent delete?',
            'swal_icon' => 'error',
            'swal_cancel' => 'Cancel',
            'swal_confirm' => 'Force Delete',
            'permission_key' => 'purge',
            'role_key' => null,
        ],
    ];
    /** @var array */
    private const DEFAULT_KEYS = [
        'class_btn',
        'class_text',
        'name',
        'icon',
        'label',
        'is_more',
        'url_method',
        //
        'toastr_title',
        'toastr_message',
        'swal_title',
        'swal_message',
        'swal_icon',
        'swal_cancel',
        'swal_confirm',
        //
        'permission_key',
        'role_key',
    ];
    public string $type;
    public string $url;
    public ?string $permissionKey;
//    public ?string $roleKey;
    private ?array $overRideDefault = null;

    private function __construct(string $type, string $url, string $permissionKey = null)
    {
        $this->type = $type;
        $this->url = $url;
        $this->permissionKey = $permissionKey;
    }

    public static function make(string $type, string $url, string $permissionKey = null)
    {
        return new static($type, $url, $permissionKey);
    }

    public function overRideDefault(array $overRideDefault)
    {
        array_map(
            function ($v) {
                if (!in_array($v, self::DEFAULT_KEYS)) {
                    throw ActionLinkException::overRideDefaultKey($v);
                }
            },
            array_keys($overRideDefault)
        );

        if (isset($overRideDefault['is_more']) && !is_bool($overRideDefault['is_more'])) {
            throw ActionLinkException::overRideDefaultIsMore($overRideDefault['is_more']);
        }

        $this->overRideDefault = $overRideDefault;

        return $this;
    }

    /**
     * @param $name
     *
     * @return string
     */
    public function __get($name)
    {
        if (in_array($name, self::DEFAULT_KEYS)) {
            return $this->getDefaultValue($name);
        }

        throw ActionLinkException::attribute($name, self::class);
    }

    private function getDefaultValue(string $name)
    {
        if (filled($this->overRideDefault) && isset($this->overRideDefault[$name])) {
            return $this->overRideDefault[$name];
        } elseif (
            isset(self::DEFAULT[$this->type]) &&
            isset(self::DEFAULT[$this->type][$name])
        ) {
            return self::DEFAULT[$this->type][$name];
        }

        // allow no value
        if ($name == 'permission_key' || $name == 'role_key') {
            return null;
        }

        throw ActionLinkException::noValue($this->type, $name);
    }
}
