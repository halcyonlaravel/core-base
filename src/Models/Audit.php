<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Models;

use HalcyonLaravelBoilerplate\Money\MoneyFacade;
use Illuminate\Database\Eloquent\Builder;
use Lloricode\LaravelHtmlTable\LaravelHtmlTableGenerator;
use Money\Money as MoneyPHP;
use Spatie\DataTransferObject\DataTransferObject;

/**
 * HalcyonLaravelBoilerplate\CoreBase\Models\Audit
 *
 * @property int $id
 * @property string|null $user_type
 * @property int|null $user_id
 * @property string $event
 * @property string $auditable_type
 * @property int $auditable_id
 * @property array|null $old_values
 * @property array|null $new_values
 * @property string|null $url
 * @property string|null $ip_address
 * @property string|null $user_agent
 * @property string|null $tags
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $auditable
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $user
 * @method static Builder|Audit newModelQuery()
 * @method static Builder|Audit newQuery()
 * @method static Builder|Audit query()
 * @method static Builder|Audit whereAuditableId($value)
 * @method static Builder|Audit whereAuditableType($value)
 * @method static Builder|Audit whereCreatedAt($value)
 * @method static Builder|Audit whereEvent($value)
 * @method static Builder|Audit whereId($value)
 * @method static Builder|Audit whereIpAddress($value)
 * @method static Builder|Audit whereNewValues($value)
 * @method static Builder|Audit whereOldValues($value)
 * @method static Builder|Audit whereTags($value)
 * @method static Builder|Audit whereUpdatedAt($value)
 * @method static Builder|Audit whereUrl($value)
 * @method static Builder|Audit whereUserAgent($value)
 * @method static Builder|Audit whereUserId($value)
 * @method static Builder|Audit whereUserType($value)
 * @mixin \Eloquent
 */
class Audit extends \OwenIt\Auditing\Models\Audit
{
    public function getOldValueTable(): string
    {
        return $this->toTable($this->getModifiedByType('old'));
    }

    public function getNewValueTable(): string
    {
        return $this->toTable($this->getModifiedByType('new'));
    }

    private function toTable(array $data): string
    {
        $multiData = [];

        foreach ($data as $field => $value) {
            $multiData[] = [
                [
                    'data' => $field,
                    'align' => 'left',
                ],
                [
                    'data' => $value,
                    'align' => 'left',
                ],
            ];
        }

        return (new LaravelHtmlTableGenerator())->generate(
            ['Field', 'Value'],
            $multiData,
            [
                'class' => 'table',
                'border' => 1,
            ]
        );
    }

    private function getModifiedByType(string $type): array
    {
        $return = [];
        foreach ($this->getModified() as $field => $data) {
            $value = $data[$type] ?? '';

            if (blank($value)) {
                $return[$field] = '';
                continue;
            }

            if (is_array($value)) {
                $value = json_encode($value);
            } elseif ($value instanceof DataTransferObject) {
                $value = json_encode($value->toArray());
            } elseif (is_bool($value)) {
                $value = $value ? 'true' : 'false';
            } elseif ($value instanceof MoneyPHP) {
                $value = MoneyFacade::formatMoneyAsCurrency($value);
            }
            $return[$field] = $value;
        }
        return $return;
    }

}
