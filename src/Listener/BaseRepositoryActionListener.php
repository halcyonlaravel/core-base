<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Listener;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Prettus\Repository\Events\RepositoryEventBase;

abstract class BaseRepositoryActionListener
{
    /**
     * @param  \Prettus\Repository\Events\RepositoryEventBase  $event
     */
    public function handle(RepositoryEventBase $event)
    {
        $modelClass = $event->getRepository()->model();
        $observers = $this->observers();

        $functionName = Str::camel($event->getAction());

        if (isset($observers[$modelClass]) &&
            class_exists($class = $observers[$modelClass]) &&
            method_exists($obj = app($class), $functionName)
        ) {
            DB::transaction(fn() => $obj->{$functionName}($event->getModel()));
        }
    }

    abstract protected function observers(): array;
}
