<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Criterion\Eloquent;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ThisWithCriteria
 *
 * @deprecated
 * @package HalcyonLaravelBoilerplate\CoreBase\Criterion\Eloquent
 */
class ThisWithCriteria implements CriteriaInterface
{
    private array $relations;

    public function __construct(array $relations)
    {
        $this->relations = $relations;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        /** @var \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Model $model */
        return $model->with($this->relations);
    }
}
