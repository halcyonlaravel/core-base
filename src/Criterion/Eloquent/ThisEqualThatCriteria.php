<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Criterion\Eloquent;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ThisEqualThatCriteria
 *
 * @package App\Criterion\Eloquent
 * @author  Lloric Mayuga Garcia <lloricode@gmail.com>
 */
class ThisEqualThatCriteria implements CriteriaInterface
{
    private $column;
    private $operator;
    private $value;
    private $boolean;

    private int $argumentCount;

    /**
     * ThisEqualThatCriteria constructor.
     *
     * @param $column
     * @param  null  $operator
     * @param  null  $value
     * @param  string  $boolean
     */
//    public function __construct(string $field, $value = null, string $comparison = '=', bool $isOrWhere = false)
    public function __construct($column, $operator = null, $value = null, $boolean = 'and')
    {
        $this->column = $column;
        $this->operator = $operator;
        $this->value = $value;
        $this->boolean = $boolean;

        $this->argumentCount = count(func_get_args());
    }

    /**
     * @param                                                   $model
     * @param  \Prettus\Repository\Contracts\RepositoryInterface  $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        /** @var \Illuminate\Database\Eloquent\Builder $model */

        switch ($this->argumentCount) {
            case 1:
                $model = $model->where($this->column);
                break;
            case 2:
                $model = $model->where($this->column, $this->operator);
                break;
            case 3:
                $model = $model->where($this->column, $this->operator, $this->value);
                break;
            case 4:
                $model = $model->where($this->column, $this->operator, $this->value, $this->boolean);
                break;
        }

        return $model;
    }
}
