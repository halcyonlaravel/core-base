<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Providers;

use HalcyonLaravelBoilerplate\CoreBase\Events\RepositoryEntityForceDeleted;
use HalcyonLaravelBoilerplate\CoreBase\Events\RepositoryEntityForceDeleting;
use HalcyonLaravelBoilerplate\CoreBase\Events\RepositoryEntityRestored;
use HalcyonLaravelBoilerplate\CoreBase\Events\RepositoryEntityRestoring;
use Prettus\Repository\Events\RepositoryEntityCreated;
use Prettus\Repository\Events\RepositoryEntityCreating;
use Prettus\Repository\Events\RepositoryEntityDeleted;
use Prettus\Repository\Events\RepositoryEntityDeleting;
use Prettus\Repository\Events\RepositoryEntityUpdated;
use Prettus\Repository\Events\RepositoryEntityUpdating;
use Prettus\Repository\Listeners\CleanCacheRepository;

class EventServiceProvider extends \Illuminate\Foundation\Support\Providers\EventServiceProvider
{
    public static function getListeners()
    {
        $repositoryActionListener = config('core-base.listeners.repository_action');
        return [
            // create
            RepositoryEntityCreating::class => [
                $repositoryActionListener,
            ],
            RepositoryEntityCreated::class => [
                $repositoryActionListener,
            ],
            // update
            RepositoryEntityUpdating::class => [
                $repositoryActionListener,
            ],
            RepositoryEntityUpdated::class => [
                $repositoryActionListener,
            ],
            // destroy
            RepositoryEntityDeleting::class => [
                $repositoryActionListener,
            ],
            RepositoryEntityDeleted::class => [
                $repositoryActionListener,
            ],
            // restore
            RepositoryEntityRestoring::class => [
                $repositoryActionListener,
            ],
            RepositoryEntityRestored::class => [
                $repositoryActionListener,
                CleanCacheRepository::class,
            ],
            // force delete
            RepositoryEntityForceDeleting::class => [
                $repositoryActionListener,
            ],
            RepositoryEntityForceDeleted::class => [
                $repositoryActionListener,
                CleanCacheRepository::class,
            ],
//        MediaHasBeenAdded::class => [
//        ],
//        CollectionHasBeenCleared::class => [
//        ],
        ];
    }

    public function listens()
    {
        return self::getListeners();
    }

    public function boot()
    {
        $this->subscribe = [

        ];
        parent::boot();
        //
    }
}
