<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Providers;

use Coderello\SocialGrant\Resolvers\SocialUserResolverInterface;
use Diglactic\Breadcrumbs\Breadcrumbs;
use HalcyonLaravelBoilerplate\CoreBase\Auth\PassportSocialResolver;
use HalcyonLaravelBoilerplate\CoreBase\Providers\Macros\BluePrintMixin;
use HalcyonLaravelBoilerplate\CoreBase\Providers\Macros\BreadcrumbsMixin;
use HalcyonLaravelBoilerplate\CoreBase\Providers\Macros\RequestMixin;
use HalcyonLaravelBoilerplate\CoreBase\Providers\Macros\RouteMixin;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use Laravel\Passport\Passport;
use Spatie\Flash\Flash;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Storage;

class CoreBaseServiceProvider extends PackageServiceProvider
{
    protected function getPackageBaseDir(): string
    {
        return __DIR__.DIRECTORY_SEPARATOR.'..';
    }

    public function configurePackage(Package $package): void
    {
        $package
            ->name('core-base')
            ->hasConfigFile(
                [
                    'core-base',
                    'core-base-permissions',
                ]
            )
            ->hasTranslations();
    }

    /**
     * @throws \ReflectionException
     */
    public function packageBooted()
    {
        if ($this->app->runningInConsole() && ! is_latest_mysql_version()) {
            // Set the default string length for Laravel5.4
            // https://laravel-news.com/laravel-5-4-key-too-long-error
            Schema::defaultStringLength(191);
        }

        Flash::levels(
            [
                'success' => 'alert-success',
                'warning' => 'alert-warning',
                'error' => 'alert-danger',
            ]
        );

        if (app()->environment('testing')) {
            foreach (array_keys(config('filesystems.disks')) as $diskName) {
                Storage::fake($diskName);
            }
        }

        Passport::cookie(Str::snake(config('app.name', 'boilerplate')).'_'.config('app.env'));
        Passport::hashClientSecrets();
        Passport::tokensExpireIn(now()->addDays(config('core-base.api.tokens_expire_in_seconds')));
        Passport::refreshTokensExpireIn(
            now()->seconds(
                config('core-base.api.refresh_tokens_expire_in_seconds')
            )
        );
//        Passport::personalAccessTokensExpireIn(now()->addMonths(6));

        $this->macros();
    }

    /**
     */
    public function packageRegistered()
    {
        $this->app->bind(SocialUserResolverInterface::class, PassportSocialResolver::class);
    }

    /**
     * @throws \ReflectionException
     */
    private function macros()
    {
        if ($this->app->runningInConsole()) {
            Blueprint::mixin(new BluePrintMixin());
        }

        Breadcrumbs::mixin(new BreadcrumbsMixin());
        Request::mixin(new RequestMixin());
        Route::mixin(new RouteMixin());
    }
}
