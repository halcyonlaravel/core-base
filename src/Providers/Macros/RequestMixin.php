<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Providers\Macros;

use Closure;
use Illuminate\Http\Request;

/**
 * Class RequestMixin
 *
 * @mixin Request
 */
class RequestMixin
{
    public function isActiveUrl(): Closure
    {
        return fn(string $routeName, ...$args): bool => request()->is(
            str_replace(config('app.url').'/', '', route($routeName, $args).'*')
        );
    }
}
