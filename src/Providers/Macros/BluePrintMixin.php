<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Providers\Macros;

use Closure;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class BluePrintMixin
 *
 * @mixin Blueprint
 */
class BluePrintMixin
{
    public function jsonable(): Closure
    {
        return function ($column) {
            $type = is_latest_mysql_version() ? 'json' : 'longText';
            return $this->$type($column);
        };
    }

    public function money(): Closure
    {
        return function ($column, string $comment = null) {
            $comment = is_null($comment) ? 'manage by moneyphp/money' : $comment.' (manage by moneyphp/money)';

            $this->unsignedInteger($column.'_amount')->comment($comment);
            $this->string($column.'_currency', 3)->comment($comment);
        };
    }

    public function moneyNullable(): Closure
    {
        return function ($column, string $comment = null) {
            $comment = is_null($comment) ? 'manage by moneyphp/money' : $comment.' (manage by moneyphp/money)';

            $this->unsignedInteger($column.'_amount')
                ->nullable()
                ->comment($comment);

            $this->string($column.'_currency', 3)
                ->nullable()
                ->comment($comment);
        };
    }
}
