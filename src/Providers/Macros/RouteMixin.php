<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Providers\Macros;

use Closure;
use HalcyonLaravelBoilerplate\CoreBase\Providers\Macros\Traits\FileHelper;
use Illuminate\Support\Facades\Route;

/**
 * Class RouteMixin
 *
 * @mixin Route
 */
class RouteMixin
{
    use FileHelper;

    public function deletedResource(): Closure
    {
        return function (string $controller, array $only = ['deleted', 'restore', 'purge']) {
            if (in_array('deleted', $only)) {
                Route::get('deleted', $controller.'@deleted')->name('deleted');
            }
            if (in_array('restore', $only)) {
                Route::patch('{id}/restore', $controller.'@restore')->name('restore');
            }
            if (in_array('purge', $only)) {
                Route::delete('{id}/deleted', $controller.'@purge')->name('purge');
            }
        };
    }

}
