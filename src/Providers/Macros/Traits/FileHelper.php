<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Providers\Macros\Traits;

use Exception;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

trait FileHelper
{
    public function requireFolder()
    {
        return function (string $folder) {
            try {
                $it = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($folder)
                );

                while ($it->valid()) {
                    if (
                        !$it->isDot() &&
                        $it->isFile() &&
                        $it->isReadable() &&
                        $it->current()->getExtension() === 'php'
                    ) {
                        require $it->key();
                    }

                    $it->next();
                }
            } catch (Exception $e) {
                dd(__METHOD__, $e->getMessage());
            }
        };
    }
}
