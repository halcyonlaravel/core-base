<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Providers\Macros;

use Closure;
use Diglactic\Breadcrumbs\Breadcrumbs as Breadcrumbs;
use Diglactic\Breadcrumbs\Generator as BreadcrumbsGenerator;
use HalcyonLaravelBoilerplate\CoreBase\Providers\Macros\Traits\FileHelper;
use Illuminate\Support\Str;

/**
 * Class BreadcrumbsMixin
 *
 * @mixin Breadcrumbs
 */
class BreadcrumbsMixin
{
    use FileHelper;

    public function resource(): Closure
    {
        return function (
            string $routePrefix,
            string $title,
            string $parent = 'backend.dashboard',
            array $only = ['index', 'show', 'create', 'edit']
        ) {
            if (in_array('index', $only)) {
                Breadcrumbs::for(
                    "$routePrefix.index",
                    function (BreadcrumbsGenerator $breadcrumbs) use ($routePrefix, $title, $parent) {
                        $breadcrumbs->parent($parent);
                        $breadcrumbs->push(Str::title($title).' Management', route("$routePrefix.index"));
                    }
                );
            }

            if (in_array('show', $only)) {
                Breadcrumbs::for(
                    "$routePrefix.show",
                    function (
                        BreadcrumbsGenerator $breadcrumbs,
                        $model
                    ) use ($routePrefix, $title, $parent) {
                        $breadcrumbs->parent("$routePrefix.index");
                        $breadcrumbs->push('Show '.Str::title($title), route("$routePrefix.show", $model));
                    }
                );
            }

            if (in_array('create', $only)) {
                Breadcrumbs::for(
                    "$routePrefix.create",
                    function (BreadcrumbsGenerator $breadcrumbs) use ($routePrefix, $title, $parent) {
                        $breadcrumbs->parent("$routePrefix.index");
                        $breadcrumbs->push('Create '.Str::title($title), route("$routePrefix.create"));
                    }
                );
            }

            if (in_array('edit', $only)) {
                Breadcrumbs::for(
                    "$routePrefix.edit",
                    function (
                        BreadcrumbsGenerator $breadcrumbs,
                        $model
                    ) use ($routePrefix, $title, $parent) {
                        $breadcrumbs->parent("$routePrefix.show", $model);
                        $breadcrumbs->push('Edit '.Str::title($title), route("$routePrefix.edit", $model));
                    }
                );
            }
        };
    }

    public function deletedResource(): Closure
    {
        return function (string $routePrefix, string $title, string $parent = 'backend.dashboard') {
            Breadcrumbs::for(
                "$routePrefix.deleted",
                function (BreadcrumbsGenerator $breadcrumbs) use ($routePrefix, $title, $parent) {
                    $breadcrumbs->parent("$routePrefix.index");
                    $breadcrumbs->push('Deleted '.Str::title($title).' List', route("$routePrefix.deleted"));
                }
            );
        };
    }

}
