<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Auth;

use Coderello\SocialGrant\Resolvers\SocialUserResolverInterface;
use Exception;
use HalcyonLaravelBoilerplate\CoreBase\Auth\Contracts\PassportSocialInterface;
use Illuminate\Contracts\Auth\Authenticatable;
use InvalidArgumentException;
use Laravel\Socialite\Facades\Socialite;
use League\OAuth2\Server\Exception\OAuthServerException;

class PassportSocialResolver implements SocialUserResolverInterface
{
    /**
     * @var \HalcyonLaravelBoilerplate\CoreBase\Auth\Contracts\PassportSocialInterface
     */
    private $userRepository;

    public function __construct()
    {
        $this->userRepository = app(config('core-base.repositories.user'));

        if (!$this->userRepository instanceof PassportSocialInterface) {
            throw new InvalidArgumentException('Repository must implemented by '.PassportSocialInterface::class);
        }
    }

    /**
     * @param  string  $provider
     * @param  string  $accessToken
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     * @throws \League\OAuth2\Server\Exception\OAuthServerException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function resolveUserByProviderCredentials(string $provider, string $accessToken): ?Authenticatable
    {
        // Return the user that corresponds to provided credentials.
        // If the credentials are invalid, then return NULL.
        if (config("services.{$provider}.active", false) === false) {
            throw OAuthServerException::invalidRequest('provider');
        }

        $providerUser = null;
        try {
            $social = Socialite::driver($provider);

            if (filled($fields = config("core-base.api.social_login.$provider.fields"))) {
                $social
                    ->fields($fields);
            }

            $providerUser = $social
                ->stateless()->userFromToken($accessToken);
        } catch (Exception $exception) {
        }

        if (blank($providerUser)) {
            return null;
        }

        return $this->userRepository->findOrCreateProvider($providerUser, $provider);
    }
}
