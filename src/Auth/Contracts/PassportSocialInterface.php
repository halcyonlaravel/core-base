<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Auth\Contracts;

use Illuminate\Contracts\Auth\Authenticatable;
use Laravel\Socialite\Two\User;

interface PassportSocialInterface
{
    /**
     * @param  \Laravel\Socialite\Two\User  $data
     * @param  string  $provider
     *
     * @return \Illuminate\Foundation\Auth\User
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function findOrCreateProvider(User $data, string $provider): Authenticatable;
}