<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Http\Middleware\Api;

use Closure;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class EnsureEmailIsVerified
{
    public function handle($request, Closure $next, $redirectToRoute = null)
    {
        /** @var \App\Models\Auth\User $user */
        $user = auth('api')->user();

        if (!$user || ($user instanceof MustVerifyEmail && !$user->hasVerifiedEmail())) {
            abort(403, 'Your email address is not verified.');
        }

        return $next($request);
    }
}
