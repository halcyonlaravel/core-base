<?php


namespace HalcyonLaravelBoilerplate\CoreBase\Http\Middleware\Api;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;

class AuthenticateMiddleware
{
    protected Auth $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     *
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($this->auth->guard('api')->guest()) {
            abort(401, 'Unauthorized');
//            return response('Unauthorized.', 401);
        }

        return $next($request);
    }
}