<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Http\Middleware\Api;

use Closure;

class EnabledUser
{
    public function handle($request, Closure $next, $redirectToRoute = null)
    {
        /** @var \App\Models\Auth\User $user */
        $user = auth('api')->user();

        if (!$user || !$user->isEnabledStatus()) {
            abort(403, 'Your account is disabled.');
        }

        return $next($request);
    }
}
