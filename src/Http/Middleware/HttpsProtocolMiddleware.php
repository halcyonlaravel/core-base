<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class HttpsProtocolMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $scheme = 'https';
        $isHttps = strpos(config('app.url'), "$scheme://") !== false;

        if (is_laravel() && $isHttps) {
            if (!$request->secure()) {
                return redirect()->secure($request->getRequestUri());
            }

            URL::forceScheme($scheme);
        } elseif ($isHttps && !$request->secure()) {
            abort(403, 'This endpoint can only be accessed with an HTTPS connection');
        }

        return $next($request);
    }
}
