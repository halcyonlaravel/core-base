<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Http\Controllers\Backend;

use HalcyonLaravelBoilerplate\CoreBase\Http\Controllers\Backend\Contracts\DeletedControllerInterface;
use HalcyonLaravelBoilerplate\CoreBase\Http\Controllers\Backend\Traits\ControllerHelperTrait;
use HalcyonLaravelBoilerplate\CoreBase\Http\Controllers\Controller;
use HalcyonLaravelBoilerplate\CoreBase\SetUp\DeletedControllerSetUp;
use HalcyonLaravelBoilerplate\MetaTag\Facades\MetaTagFacade as MetaTag;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

abstract class DeletedController extends Controller implements DeletedControllerInterface
{
    use ControllerHelperTrait;

    protected DeletedControllerSetUp $setUp;

    public function __construct()
    {
        $this->setUp = $this->setUp();

        foreach ($this->setUp->permissions as $permission) {
            $this->middleware('permission:'.$this->setUp->modelPermissions($permission))->only(Str::camel($permission));
        }
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @deprecated Moving in livewire datatable filters
     */
    public function deleted(Request $request)
    {
        $this->authorize('viewAny', get_class($this->setUp->getModel()));

        MetaTag::setTags(
            [
                'title' => 'Deleted '.Str::title($this->setUp->title).' Management',
            ]
        );

        return view($this->setUp->viewBackendPath.'.deleted');
    }

    /**
     * @param  string  $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function restore(string $id)
    {
        /** @var \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\SoftDeletes $MODEL */
        $MODEL = $this->setUp->getModel();

        /** @var \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\SoftDeletes $model */
        $model = $MODEL->query()
            ->where($MODEL->getRouteKeyName(), $id)
            ->withTrashed()
            ->firstOrFail();

        $this->authorize('restore', $model);

        $model->restore();

        return response()
            ->json(
                [
                    'message' => 'success',
                    'status' => 200,
                ]
            )->setStatusCode(200);
    }

    /**
     * @param  string  $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \Throwable
     */
    public function purge(string $id)
    {
        /** @var \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\SoftDeletes $MODEL */
        $MODEL = $this->setUp->getModel();

        /** @var \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\SoftDeletes $model */
        $model = $MODEL->query()
            ->where($MODEL->getRouteKeyName(), $id)
            ->withTrashed()
            ->firstOrFail();

        $this->authorize('forceDelete', $model);

        $model->forceDelete();

        return response()
            ->json()->setStatusCode(204);
    }
}
