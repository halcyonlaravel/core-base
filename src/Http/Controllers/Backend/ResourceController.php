<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Http\Controllers\Backend;

use HalcyonLaravelBoilerplate\CoreBase\Http\Controllers\Backend\Contracts\ResourceControllerInterface;
use HalcyonLaravelBoilerplate\CoreBase\Http\Controllers\Backend\Traits\ControllerHelperTrait;
use HalcyonLaravelBoilerplate\CoreBase\Http\Controllers\Controller;
use HalcyonLaravelBoilerplate\CoreBase\Models\Contracts\ResourceConfigInterface;
use HalcyonLaravelBoilerplate\CoreBase\SetUp\ResourceControllerSetUp;
use HalcyonLaravelBoilerplate\MetaTag\Facades\MetaTagFacade as MetaTag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

abstract class ResourceController extends Controller implements ResourceControllerInterface
{
    use ControllerHelperTrait;

    /** @var string[] */
    protected array $with = [];
    /** @var string[] */
    protected array $withCount = [];

    protected ResourceControllerSetUp $setUp;

    public function __construct()
    {
        $this->setUp = $this->setUp();

        foreach ($this->setUp->permissions as $permission) {
            $middleware = $this->middleware('permission:'.$this->setUp->modelPermissions($permission));

            $m = [Str::camel($permission)];

            switch ($permission) {
                case 'create':
                    $m[] = 'store';
                    break;
                case 'edit':
                    $m[] = 'update';
                    break;
            }

            $middleware->only($m);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', get_class($this->setUp->getModel()));

        MetaTag::setTags(
            [
                'title' => trans($this->setUp->tabTitleTransIndex, ['model' => $this->setUp->title]),
            ]
        );

        return view(
            $this->setUp->viewBackendPath.'.index',
            [
                'MODEL' => $this->setUp->getModel(),
            ]
        );
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', get_class($this->setUp->getModel()));

        MetaTag::setTags(
            [
                'title' => trans($this->setUp->tabTitleTransCreate, ['model' => $this->setUp->title]),
            ]
        );

        return view($this->setUp->viewBackendPath.'.create');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Laravel\Lumen\Http\Redirector|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $this->authorize('create', get_class($this->setUp->getModel()));

        $validationOptions = $this->storeValidation($request);
        $validatedAttributes = $this->validate(
            $request,
            $validationOptions->rules,
            $validationOptions->messages,
            $validationOptions->customAttributes
        );

        $model = $this->createAction($this->resourceGenerateStub($validatedAttributes, $request));

        $message = trans(
            $this->setUp->transStoreSuccessMessage,
            [
                'model' => $this->setUp->title,
            ]
        );

        flash($message, 'success');

        if ($url = $this->setUp->storeRedirectUrl) {
            return redirect($url);
        }

        return redirect(route($this->setUp->routeBackendPath.'.edit', $model));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show($id)
    {
        MetaTag::setTags(
            [
                'title' => trans($this->setUp->tabTitleTransShow, ['model' => $this->setUp->title]),
            ]
        );

        $model = $this->find($id);

        $this->authorize('view', $model);

        return view($this->setUp->viewBackendPath.'.show', compact('model'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id)
    {
        MetaTag::setTags(
            [
                'title' => trans($this->setUp->tabTitleTransEdit, ['model' => $this->setUp->title]),
            ]
        );

        $model = $this->find($id);

        $this->authorize('update', $model);

        return view($this->setUp->viewBackendPath.'.edit', compact('model'));
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Laravel\Lumen\Http\Redirector
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Throwable
     */
    public function update(Request $request, $id)
    {
        $model = $this->find($id);

        $this->authorize('update', $model);

        $validationOptions = $this->updateValidation($request, $model);
        $validatedAttributes = $this->validate(
            $request,
            $validationOptions->rules,
            $validationOptions->messages,
            $validationOptions->customAttributes
        );

        $model = $this->editAction($model, $this->resourceGenerateStub($validatedAttributes, $request, $model));

        $message = trans(
            $this->setUp->transUpdateSuccessMessage,
            [
                'model' => $this->setUp->title,
            ]
        );

        flash($message, 'success');

        if ($url = $this->setUp->updateRedirectUrl) {
            return redirect($url);
        }

        return redirect(route($this->setUp->viewBackendPath.'.edit', $model->refresh()));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function destroy($id)
    {
        $model = $this->find($id);

        $this->authorize('delete', $model);

        foreach ($this->setUp->checkChildRelationOnDestroy as $relation) {
            $relatedMorphClass = $model->{$relation}()->getRelated()->getMorphClass();
            $relatedMorphClass = Relation::getMorphedModel($relatedMorphClass) ?? $relatedMorphClass;

            $isHasSoftDelete = is_class_uses_deep(new $relatedMorphClass(), SoftDeletes::class);

            if ($isHasSoftDelete) {
                $count = $model->{$relation}()->withTrashed()->count();
            } else {
                $count = $model->{$relation}()->count();
            }

            if ($count > 0) {
                $name = '';

                if ($model instanceof ResourceConfigInterface) {
                    $name = ' '.Str::lower(Str::singular($model::resourceModelName()));
                }

//                if($isHasSoftDelete)
//                    $relation = 'soft deleted '.$relation;

                $message = "Failed to delete{$name}, it has data child on $relation";

                if (isset($this->translationMessageOnFailedDestroy[$relation])) {
                    $message = trans($this->setUp->translationMessageOnFailedDestroy[$relation]);
                }

                return response()
                    ->json(
                        [
                            'message' => $message,
                            'status' => 422,
                        ]
                    )->setStatusCode(422);
            }
        }

        $this->deleteAction($model);

        return response()
            ->json()->setStatusCode(204);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function find($id): Model
    {
        $MODEL = $this->setUp->getModel();

        $model = $MODEL->query()->where($MODEL->getRouteKeyName(), $id);

        if (filled($this->with)) {
            $model->with($this->with);
        }

        if (filled($this->withCount)) {
            $model->withCount($this->withCount);
        }

        return $model->firstOrFail();
    }

    protected function createAction(array $attributes): Model
    {
        if ($this->setUp->model !== null) {
            return DB::transaction(fn() => $this->setUp->getModel()->create($attributes));
        }

        return DB::transaction(fn() => $this->setUp->repositoryInterface->create($attributes));
    }

    protected function editAction(Model $model, array $attributes): Model
    {
        if ($this->setUp->model !== null) {
            return DB::transaction(fn() => $model->update($attributes));
        }

        return DB::transaction(fn() => $this->setUp->repositoryInterface->update($attributes, $model->getKey()));
    }

    protected function deleteAction(Model $model): void
    {
        if ($this->setUp->model !== null) {
            DB::transaction(fn() => $model->delete());
        }

        DB::transaction(fn() => $this->setUp->repositoryInterface->delete($model->getKey()));
    }
}
