<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Http\Controllers\Backend\Traits;

use HalcyonLaravelBoilerplate\CoreBase\Criterion\Eloquent\ThisEqualThatCriteria;
use HalcyonLaravelBoilerplate\CoreBase\Criterion\Eloquent\ThisScopeCriteria;
use HalcyonLaravelBoilerplate\CoreBase\Criterion\Eloquent\WithTrashCriteria;
use HalcyonLaravelBoilerplate\CoreBase\Repository\Eloquent\BaseRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

trait ControllerHelperTrait
{
    /**
     * @param  \HalcyonLaravelBoilerplate\CoreBase\Repository\Eloquent\BaseRepositoryInterface  $repo
     * @param  string  $key
     * @param  bool  $trash
     * @param  array|null  $customWhere
     * @param  string|null  $queryScope
     *
     * @return mixed
     * @deprecated
     */
    public function getModel(
        BaseRepositoryInterface $repo,
        string $key,
        bool $trash = false,
        array $customWhere = null,
        string $queryScope = null
    ) {
        $modelClass = $repo->makeModel();

        $where = [
            $modelClass->getRouteKeyName() => $key,
        ];

        if (!is_null($customWhere)) {
            $where = array_merge($where, $customWhere);
        }

        if (!is_null($queryScope)) {
            $repo->pushCriteria(new ThisScopeCriteria($queryScope));
        }

        if ($trash && method_exists($modelClass, 'bootSoftDeletes')) {
            $repo->pushCriteria(new WithTrashCriteria());
        }

        foreach ($where as $field => $value) {
            $repo->pushCriteria(new ThisEqualThatCriteria($field, $value));
        }

        $model = $repo->all()->first();

        if (is_null($model)) {
            throw new ModelNotFoundException($model);
        }

        return $model;
    }
}
