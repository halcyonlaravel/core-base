<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Http\Controllers\Backend\Contracts;

use HalcyonLaravelBoilerplate\CoreBase\SetUp\ResourceControllerSetUp;
use HalcyonLaravelBoilerplate\CoreBase\SetUp\Validators\Validation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface ResourceControllerInterface
{
    /**
     * @return \HalcyonLaravelBoilerplate\CoreBase\SetUp\ResourceControllerSetUp
     */
    public function setUp(): ResourceControllerSetUp;

    /**
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \HalcyonLaravelBoilerplate\CoreBase\SetUp\Validators\Validation
     */
    public function storeValidation(Request $request): Validation;

    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Model  $model
     *
     * @return \HalcyonLaravelBoilerplate\CoreBase\SetUp\Validators\Validation
     */
    public function updateValidation(Request $request, Model $model): Validation;

    /**
     * @param  array  $validatedAttributes
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Model|null  $model
     *
     * @return array
     */
    public function resourceGenerateStub(array $validatedAttributes, Request $request, Model $model = null): array;
}
