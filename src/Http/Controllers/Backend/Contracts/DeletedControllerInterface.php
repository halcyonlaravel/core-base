<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Http\Controllers\Backend\Contracts;

use HalcyonLaravelBoilerplate\CoreBase\SetUp\DeletedControllerSetUp;

interface DeletedControllerInterface
{
    /**
     * @return \HalcyonLaravelBoilerplate\CoreBase\SetUp\DeletedControllerSetUp
     */
    public function setUp(): DeletedControllerSetUp;

}
