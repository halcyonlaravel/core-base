<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Http\Controllers\Backend\Contracts;

use HalcyonLaravelBoilerplate\CoreBase\SetUp\TableControllerSetUp;

interface TableControllerInterface
{
    /**
     * @return \HalcyonLaravelBoilerplate\CoreBase\SetUp\TableControllerSetUp
     */
    public function setUp(): TableControllerSetUp;

}
