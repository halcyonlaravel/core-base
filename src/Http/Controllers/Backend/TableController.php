<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Http\Controllers\Backend;

use Closure;
use HalcyonLaravelBoilerplate\CoreBase\Criterion\Eloquent\OnlyTrashedCriteria;
use HalcyonLaravelBoilerplate\CoreBase\Http\Controllers\Backend\Contracts\TableControllerInterface;
use HalcyonLaravelBoilerplate\CoreBase\Http\Controllers\Controller;
use HalcyonLaravelBoilerplate\CoreBase\Models\ActionLinks\Link;
use HalcyonLaravelBoilerplate\CoreBase\SetUp\TableControllerSetUp;
use Illuminate\Http\Request;

/**
 * @deprecated Use livewire datatable instead, this will remove on next major release.
 */
abstract class TableController extends Controller implements TableControllerInterface
{
    protected TableControllerSetUp $setUp;

    public function __construct()
    {
        $this->setUp = $this->setUp();

        $this->middleware('permission:'.$this->setUp->modelPermissions('index'))
            ->only($this->setUp->index);
    }

    /**
     * @param  array<int, string>  $select
     *
     * @return \Yajra\DataTables\DataTableAbstract
     * @throws \Exception
     * @deprecated use datatableUsingModel()
     */
    protected function datatable(
        Request $request,
        array $select = ['*'],
        Closure $closure = null,
        Closure $closureFetch = null
    ) {
        $repo = $this->setUp
            ->repositoryInterface;

        if (!is_null($closure)) {
            $closure($repo);
        }

        if ($request->has('trash') && $request->input('trash') == true) {
            if (!auth()->user()->can($this->setUp->modelPermissions('deleted'))) {
                abort(403);
            }

            $repo->pushCriteria(new OnlyTrashedCriteria());
        }

        if (!is_null($closureFetch)) {
            return datatables()->of(
                $closureFetch($repo)
            );
        }

        return datatables()->of(
            $repo->select($select)
        );
    }

    /**
     * @param  array<int, string>  $select
     *
     * @return \Yajra\DataTables\DataTableAbstract
     * @throws \Exception
     */
    protected function datatableUsingModel(
        Request $request,
        array $select = ['*'],
        Closure $closure = null,
        Closure $closureFetch = null
    ) {
        $MODEL = $this->setUp->getModel()->query();

        if (!is_null($closure)) {
            $closure($MODEL);
        }

        if ($request->has('trash') && $request->input('trash') == true) {
            if (!auth()->user()->can($this->setUp->modelPermissions('deleted'))) {
                abort(403);
            }

            $MODEL->onlyTrashed();
        }

        if (!is_null($closureFetch)) {
            return datatables()->of(
                $closureFetch($MODEL)
            );
        }

        return datatables()->of(
            $MODEL->select($select)
        );
    }

    protected function actions($links)
    {
        /** @var Link[] $links */

        $return = '';
        $more = '';

        foreach ($links as $link) {
            switch ($link->url_method) {
                case 'get':

                    if ($link->is_more) {
                        $more .= html()->a($link->url, $link->label)
                            ->attributes(
                                [
                                    'class' => 'btn btn-sm dropdown-item',
                                    'data-url' => $link->url,
                                    'data-url-method' => $link->url_method,
                                ]
                            );
                    } else {
                        $return .= html()->a($link->url, '<i class="'.$link->icon.'"></i>')
                            ->class("btn {$link->class_btn} btn-sm");
                    }

                    break;
                case 'delete':
                case 'patch':
                case 'post':

                    if ($link->is_more) {
                        $more .= html()->button($link->label)
                            ->attributes(
                                [
                                    'class' => 'btn btn-sm dropdown-item action-btn',
                                    'data-url' => $link->url,
                                    'data-url-method' => $link->url_method,
                                    //
                                    'data-toastr-title' => $link->toastr_title,
                                    'data-toastr-message' => $link->toastr_message,
                                    'data-swal-title' => $link->swal_title,
                                    'data-swal-message' => $link->swal_message,
                                    'data-swal-icon' => $link->swal_icon,
                                    'data-swal-cancel' => $link->swal_cancel,
                                    'data-swal-confirm' => $link->swal_confirm,
                                ]
                            );
                    } else {
                        $return .= html()->button('<i class="'.$link->icon.'"></i>')
                            ->attributes(
                                [
                                    'class' => "btn {$link->class_btn} btn-sm action-btn",
                                    'data-url' => $link->url,
                                    'data-url-method' => $link->url_method,
                                    //
                                    'data-toastr-title' => $link->toastr_title,
                                    'data-toastr-message' => $link->toastr_message,
                                    'data-swal-title' => $link->swal_title,
                                    'data-swal-message' => $link->swal_message,
                                    'data-swal-icon' => $link->swal_icon,
                                    'data-swal-cancel' => $link->swal_cancel,
                                    'data-swal-confirm' => $link->swal_confirm,
                                ]
                            );
                    }

                    break;
            }
        }

        if (!empty($more)) {
            $return .= html()->button(
                    html()->a(null, 'More')
                )
                    ->attributes(
                        [
                            'class' => 'btn btn-secondary btn-sm dropdown-toggle',
                            'data-toggle' => 'dropdown',
                            'role' => 'button',
                            'aria-haspopup' => 'true',
                            'aria-expanded' => 'false',
                            'id' => 'action_btn',
                        ]
                    ).
                html()->div($more)->attributes(
                    [
                        'class' => 'dropdown-menu',
                        'aria-labelledby' => 'action_btn',
                    ]
                );
        }

        return $return;
    }
}
