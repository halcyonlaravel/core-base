<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Http\Controllers\API;

use HalcyonLaravelBoilerplate\CoreBase\Http\Controllers\Controller as BaseController;
use HalcyonLaravelBoilerplate\CoreBase\Transformers\BaseTransformer;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Response;
use League\Fractal\Serializer\JsonApiSerializer;
use Spatie\Fractal\Fractal;

abstract class Controller extends BaseController
{
    /**
     * @param  array  $data
     *
     * @return \Illuminate\Http\Response
     */
    protected function validationErrorResponse(array $data): Response
    {
        return (new Response(['errors' => $data]))->setStatusCode(422);
    }

    public function user(string $guard = 'api'): ?Authenticatable
    {
        $auth = auth()->guard($guard);
        return $auth->check()
            ? $auth->user()
            : null;
    }

    /**
     * @param $data
     * @param  \HalcyonLaravelBoilerplate\CoreBase\Transformers\BaseTransformer  $transformer
     * @param  string  $serializer
     *
     * @return \Spatie\Fractal\Fractal
     */
    protected static function fractal(
        $data,
        BaseTransformer $transformer,
        string $serializer = JsonApiSerializer::class
    ): Fractal {
        return fractal($data, $transformer, $serializer)
            ->withResourceName($transformer->getResourceKey())
            ->addMeta(['include' => $transformer->getAvailableIncludes()]);
    }

    /**
     * @param  string  $message
     * @param  int  $status
     *
     * @return \Illuminate\Http\Response
     */
    protected static function formattedSingleStatusResponse(string $message, int $status = 200): Response
    {
        return new Response(
            [
                'message' => $message,
                'status' => $status,
            ], $status
        );
    }

    /**
     * @param  array  $data
     *
     * @return \Illuminate\Http\Response
     */
    protected static function arrayResponse(array $data): Response
    {
        return new Response($data);
    }
}

