<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Testing\Traits;

use Illuminate\Http\UploadedFile;

trait ImageSeederHelper
{

    public function imgFile(string $file): UploadedFile
    {
        return new UploadedFile(test_file_path($file), $file);
    }
}
