<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Testing\Traits;

use HalcyonLaravelBoilerplate\CoreBase\Models\Contracts\PermissionInterface;
use InvalidArgumentException;

trait PermissionSeederHelper
{
    /**
     * @param  \HalcyonLaravelBoilerplate\CoreBase\Models\Contracts\PermissionInterface  $modelClass
     * @param  array  $custom
     * @param  string  $adminKey
     */
    public function seederPermission(
        PermissionInterface $modelClass,
        array $custom = [],
        string $adminKey = 'admin'
    ) {
        $this->permissionStore($modelClass, $custom, $adminKey);
    }

    /**
     * @param  array  $permissionName
     * @param  string  $adminKey
     */
    public function seederPermissionArray(array $permissionName, string $adminKey = 'admin')
    {
        $this->permissionStore($permissionName, [], $adminKey);
    }

    /**
     * @param $modelClassOrArray
     * @param  array  $custom
     * @param  string  $adminKey
     */
    private function permissionStore($modelClassOrArray, array $custom = [], string $adminKey = 'admin')
    {
//        if ( ! app()->environment('testing')) {
//            if ($modelClassOrArray instanceof PermissionInterface) {
//                print_r(get_class($modelClassOrArray)."\n");
//            } else {
//                print_r('ARRAY DATA'."\n");
//            }
//        }

        $permissionNames = $modelClassOrArray instanceof PermissionInterface ?
            $modelClassOrArray::accessPermissions()
            : $modelClassOrArray;

//        if ( ! app()->environment('testing')) {
//            print_r(
//                [
//                    'permission_names' => $permissionNames,
//                    'custom' => $custom,
//                    'admin_key' => $adminKey,
//                ]
//            );
//        }

        /** @var \Spatie\Permission\Models\Role $roleModel */
        $roleModel = app(config('permission.models.role'));

        /** @var \Spatie\Permission\Models\Permission $permissionModel */
        $permissionModel = app(config('permission.models.permission'));

        $config = config('core-base-permissions.roles');

        array_map(
            function (string $permissionName) use ($permissionModel) {
                $permissionModel::findOrCreate($permissionName);
            },
            $permissionNames
        );

//            https://github.com/spatie/laravel-permission/wiki/Global-%22Admin%22-role
//            $roleModel::findByName($config['system_role'])->givePermissionTo($permission);

        array_map(
            function (string $permissionName) use ($roleModel, $config, $adminKey) {
                $roleModel::findByName($config[$adminKey])
                    ->givePermissionTo($permissionName);
            },
            $this->getOnlyPermissions($permissionNames, $custom)
        );

//        if ( ! app()->environment('testing')) {
//            print_r("\n");
//        }
    }

    private function getOnlyPermissions(array $permissionNames, array $custom = []): array
    {
        if (blank($custom)) {
            return $permissionNames;
        }

        $valid = ['only', 'except'];

        if (count($custom) != 1) {
            throw new InvalidArgumentException('must only one key: '.implode(' or ', $valid));
        }

        $collection = collect($permissionNames);

        $key = array_keys($custom)[0];

        $condition = fn($permissionName, $index) => in_array($index, $custom[$key]);

        switch ($key) {
            case $valid[0]:
                $collection = $collection
                    ->filter($condition);
                break;

            case $valid[1]:
                $collection = $collection
                    ->reject($condition);
                break;

            default :
                throw new InvalidArgumentException('allowed key is '.implode(',', $valid).', given: '.$key);
        }

        return $collection->toArray();
    }
}
