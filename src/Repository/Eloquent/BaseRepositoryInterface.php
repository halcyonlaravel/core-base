<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface BaseRepositoryInterface
 *
 * @method BaseRepository pushCriteria(CriteriaInterface $param)
 * @method Model makeModel()
 * @method string model()
 * @mixin Builder
 * @mixin Model
 * @deprecated use direct model
 */
interface BaseRepositoryInterface extends RepositoryInterface
{
    /**
     * @return \Illuminate\Database\Eloquent\Builder
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function query();

    /**
     * @param $id
     *
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function restore($id): Model;

    /**
     * @param $id
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \Throwable
     */
    public function purge($id);

    /**
     * @param  string  $key
     * @param  bool  $isThrow404
     * @param  bool  $isCache
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function findModel(string $key, bool $isThrow404 = true, bool $isCache = true): ?Model;

}
