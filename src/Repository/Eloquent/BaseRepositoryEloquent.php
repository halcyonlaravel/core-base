<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Repository\Eloquent;

use HalcyonLaravelBoilerplate\CoreBase\Criterion\Eloquent\OnlyTrashedCriteria;
use HalcyonLaravelBoilerplate\CoreBase\Criterion\Eloquent\ThisEqualThatCriteria;
use HalcyonLaravelBoilerplate\CoreBase\Events\RepositoryEntityForceDeleted;
use HalcyonLaravelBoilerplate\CoreBase\Events\RepositoryEntityForceDeleting;
use HalcyonLaravelBoilerplate\CoreBase\Events\RepositoryEntityRestored;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/** @deprecated use direct model */
abstract class BaseRepositoryEloquent extends BaseRepository
{
    use CacheableRepository {
        paginate as public copyPaginate;
    }

    /**
     * @param  string  $key
     * @param  bool  $isThrow404
     * @param  bool  $isCache
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function findModel(string $key, bool $isThrow404 = true, bool $isCache = true): ?Model
    {
        $this->pushCriteria(
            new ThisEqualThatCriteria(
                $this->makeModel()->getRouteKeyName(), $key
            )
        );

        $model = $this->{$isCache ? 'all' : 'get'}()->first();
        if ($isThrow404 && blank($model)) {
            abort(404);
        }

        return $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function query()
    {
        $this->applyCriteria();
        $this->applyScope();

        $query = $this->model->query();

        $this->resetModel();

        return $query;
    }

    /**
     * @param  null  $limit
     * @param  array  $columns
     * @param  string  $method
     *
     * @return mixed
     */
    public function paginate($limit = null, $columns = ['*'], $method = "paginate")
    {
        // ignore all when limit already specify
        if (! is_null($limit)) {
            return $this->copyPaginate($limit, $columns, $method);
        }

        $coreBaseConfig = config('core-base.api');
        $requestLimit = app('request')->get('limit');

        if (! is_null($requestLimit)) {
            $limit = (
                $requestLimit >= 0 &&
                $requestLimit <= $coreBaseConfig['limit_pagination']
            )
                ? $requestLimit
                : null;
        }

        if ($limit == '0' && $coreBaseConfig['skip_pagination'] === true) {
            return $this->all($columns);
        }

        if (!is_null($limit)) {
            $limit = (int)$limit;
        }

        return $this->copyPaginate($limit, $columns, $method);
    }

    /**
     * @return array
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getFieldsSearchable()
    {
        $model = $this->makeModel();
        $tableColumns = $model->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($model->getTable());

        $fieldSearchable = array_map(
            fn() => 'like',
            array_flip($tableColumns)
        );

        Arr::forget($fieldSearchable, ['id', 'created_at', 'updated_at', 'deleted_at']);

        return parent::getFieldsSearchable() + collect($fieldSearchable)
                ->filter(
                    function ($value, $key) {
                        // polymorphic
                        foreach (['_id', '_type'] as $exclude) {
                            if ($exclude == substr($key, strlen($key) - strlen($exclude), strlen($key))) {
                                return false;
                            }
                        }

                        return true;
                    }
                )->toArray();
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function restore($id): Model
    {
        $this->pushCriteria(new OnlyTrashedCriteria());
        $model = $this->find($id);

        $model->restore();

        event(new RepositoryEntityRestored($this, $model));

        return $model;
    }

    /**
     * @param $id
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \Throwable
     */
    public function purge($id)
    {
        $this->pushCriteria(new OnlyTrashedCriteria());
        $model = $this->find($id);
        $cloneModel = clone $model;

        event(new RepositoryEntityForceDeleting($this, $model));

        $model->forceDelete();

        event(new RepositoryEntityForceDeleted($this, $cloneModel));
    }

}
