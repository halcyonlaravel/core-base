<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Events;

use Prettus\Repository\Events\RepositoryEventBase;

class RepositoryEntityForceDeleted extends RepositoryEventBase
{
    protected $action = 'force deleted';
}