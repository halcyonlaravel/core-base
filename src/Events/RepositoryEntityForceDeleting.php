<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Events;

use Prettus\Repository\Events\RepositoryEventBase;

class RepositoryEntityForceDeleting extends RepositoryEventBase
{
    protected $action = 'force deleting';
}