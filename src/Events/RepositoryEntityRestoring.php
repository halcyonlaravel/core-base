<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Events;

use Prettus\Repository\Events\RepositoryEventBase;

class RepositoryEntityRestoring extends RepositoryEventBase
{
    protected $action = 'restoring';
}