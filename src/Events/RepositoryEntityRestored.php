<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Events;

use Prettus\Repository\Events\RepositoryEventBase;

class RepositoryEntityRestored extends RepositoryEventBase
{
    protected $action = 'restored';
}