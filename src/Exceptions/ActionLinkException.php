<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Exceptions;

use InvalidArgumentException;

class ActionLinkException extends InvalidArgumentException
{
    public static function side(string $side)
    {
        return new static("Argument side `{$side}` not found on model action links.");
    }

    public static function type(string $type)
    {
        return new static("Argument type `{$type}` not found on model action links.");
    }

    public static function overRideDefaultKey(string $key)
    {
        return new static("Invalid key `{$key}` for overRideDefault in actionLinks.");
    }

    public static function attribute(string $name, string $currentClassName)
    {
        return new static("Attribute `{$name}` not found in $currentClassName");
    }

    public static function overRideDefaultIsMore(string $givenValue)
    {
        $type = gettype($givenValue);
        return new static("Key `is_more` in action link must be bool, `{$type}` given");
    }

    public static function noValue(string $type, string $name)
    {
        return new static("No value for type `{$type}` and attribute `{$name}`");
    }
}