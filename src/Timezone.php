<?php

namespace HalcyonLaravelBoilerplate\CoreBase;

use Carbon\Carbon;

final class Timezone
{
    public static function convertToLocal(Carbon $datetime, $userColumn = 'timezone'): Carbon
    {
        $datetime->setTimezone(self::resolveUserTimezone($userColumn));

        return $datetime;
    }

    public static function convertFromLocal($datetime, $userColumn = 'timezone'): Carbon
    {
        return now()->parse($datetime, self::resolveUserTimezone($userColumn))->setTimezone(config('app.timezone'));
    }

    public static function format(
        Carbon $datetime,
        ?string $format = 'datetime_12',
        bool $isIncludeTimezone = false
    ) {
        if (in_array($format, ['date', 'time_12', 'time_24', 'datetime_12', 'datetime_24',])) {
            $format = config('core-base.formats.'.$format);
        }

        $formattedDatetime = $datetime->format($format ?? config('core-base.formats.datetime_12'));

        if ($isIncludeTimezone) {
            $timezone = $datetime->format('e');
            $parts = explode('/', $timezone);

            if (count($parts) > 1) {
                $timezone = str_replace('_', ' ', $parts[1]).', '.$parts[0];
            } else {
                $timezone = str_replace('_', ' ', $parts[0]);
            }
            $formattedDatetime .= ' '.$timezone;
        }

        return $formattedDatetime;
    }

    private static function resolveUserTimezone($userColumn = 'timezone'): string
    {
        $auth = app('auth');
        return $auth->check()
            ? $auth->user()->$userColumn ?? config('app.timezone')
            : config('app.timezone');
    }
}
