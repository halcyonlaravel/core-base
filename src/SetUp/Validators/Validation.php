<?php

namespace HalcyonLaravelBoilerplate\CoreBase\SetUp\Validators;

/**
 * Class Validation
 *
 * @package HalcyonLaravelBoilerplate\CoreBase\SetUp\Validators
 */
class Validation
{
    public array $rules;
    public array $messages;
    public array $customAttributes;

    /**
     * @return static
     */
    public static function create(): self
    {
        return (new static())->reset();
    }

    /**
     * @return $this
     */
    public function reset(): self
    {
        $this->rules = [];
        $this->customAttributes = [];
        $this->messages = [];

        return $this;
    }

    /**
     * @param  array  $rules
     *
     * @return $this
     */
    public function rules(array $rules): self
    {
        $this->rules = $rules;

        return $this;
    }

    /**
     * @param  array  $storeCustomAttributes
     *
     * @return $this
     */
    public function customAttributes(array $storeCustomAttributes): self
    {
        $this->customAttributes = $storeCustomAttributes;

        return $this;
    }

    /**
     * @param  array  $messages
     *
     * @return $this
     */
    public function messages(array $messages): self
    {
        $this->messages = $messages;

        return $this;
    }

}
