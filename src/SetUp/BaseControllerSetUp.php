<?php

namespace HalcyonLaravelBoilerplate\CoreBase\SetUp;

use HalcyonLaravelBoilerplate\CoreBase\Models\Contracts\PermissionInterface;
use HalcyonLaravelBoilerplate\CoreBase\Repository\Eloquent\BaseRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use InvalidArgumentException;

abstract class BaseControllerSetUp
{
    /**
     * @deprecated use $model
     */
    public ?BaseRepositoryInterface $repositoryInterface = null;
    public ?string $model = null;
    public array $permissions;

    /**
     * @return static
     */
    public static function create(): self
    {
        return (new static())->reset();
    }

    abstract public function reset(): BaseControllerSetUp;

    /** @deprecated Use model() instead */
    public function repositoryInterface(string $repositoryInterface): self
    {
        $this->repositoryInterface = app($repositoryInterface);
        return $this;
    }
    public function model(string $model): self
    {
        $this->model = $model;
        return $this;
    }

    public function permissions(array $permissions): self
    {
        $this->permissions = $permissions;

        return $this;
    }

    /**
     * @return array|mixed
     */
    public function modelPermissions(?string $key = null)
    {
        $MODEL = $this->getModel();
        if ($MODEL instanceof PermissionInterface) {
            $values = $MODEL::accessPermissions();

            if (is_null($key)) {
                return $values;
            } elseif (isset($values[$key])) {
                return $values[$key];
            }
            throw new InvalidArgumentException(get_class($MODEL)." has no permission key [$key].");
        }
        throw new InvalidArgumentException(get_class($MODEL).' must implemented by '.PermissionInterface::class);
    }

    public function getModel(): Model
    {
        $model = $this->model;

        if ($model === null) {
            return $this->repositoryInterface->makeModel();
        }

        return new $model();
    }
}
