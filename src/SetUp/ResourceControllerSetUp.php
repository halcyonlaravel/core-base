<?php

namespace HalcyonLaravelBoilerplate\CoreBase\SetUp;

use HalcyonLaravelBoilerplate\CoreBase\Models\Contracts\ResourceConfigInterface;

/**
 * @property string title
 * @property string viewBackendPath
 * @property string routeBackendPath
 */
class ResourceControllerSetUp extends BaseControllerSetUp
{
    public array $checkChildRelationOnDestroy = [];
    public array $translationMessageOnFailedDestroy = [];
    public string $transStoreSuccessMessage = ':Model successfully created!';
    public string $transUpdateSuccessMessage = ':Model successfully updated!';
    public string $tabTitleTransIndex = ':Model Management';
    public string $tabTitleTransCreate = 'Create :Model';
    public string $tabTitleTransShow = 'Show :Model';
    public string $tabTitleTransEdit = 'Edit :Model';
    private array $attributes;

    public ?string $storeRedirectUrl = null;
    public ?string $updateRedirectUrl = null;

    public function storeRedirectUrl(string $storeRedirectUrl): self
    {
        $this->storeRedirectUrl = $storeRedirectUrl;
        return $this;
    }

    public function updateRedirectUrl(string $updateRedirectUrl): self
    {
        $this->updateRedirectUrl = $updateRedirectUrl;
        return $this;
    }

    public function tabTitleTransIndex(string $tabTitleTransIndex): self
    {
        $this->tabTitleTransIndex = $tabTitleTransIndex;
        return $this;
    }

    public function tabTitleTransCreate(string $tabTitleTransCreate): self
    {
        $this->tabTitleTransCreate = $tabTitleTransCreate;
        return $this;
    }

    public function tabTitleTransShow(string $tabTitleTransShow): self
    {
        $this->tabTitleTransShow = $tabTitleTransShow;
        return $this;
    }

    public function tabTitleTransEdit(string $tabTitleTransEdit): self
    {
        $this->tabTitleTransEdit = $tabTitleTransEdit;
        return $this;
    }

    public function checkChildRelationOnDestroy(array $checkChildRelationOnDestroy): self
    {
        $this->checkChildRelationOnDestroy = $checkChildRelationOnDestroy;
        return $this;
    }

    public function translationMessageOnFailedDestroy(array $translationMessageOnFailedDestroy): self
    {
        $this->translationMessageOnFailedDestroy = $translationMessageOnFailedDestroy;
        return $this;
    }

    public function transStoreSuccessMessage(string $transStoreSuccessMessage): self
    {
        $this->transStoreSuccessMessage = $transStoreSuccessMessage;
        return $this;
    }

    public function transUpdateSuccessMessage(string $transUpdateSuccessMessage): self
    {
        $this->transUpdateSuccessMessage = $transUpdateSuccessMessage;
        return $this;
    }


    /**
     * @return $this
     */
    public function reset(): BaseControllerSetUp
    {
        $this->attributes = [
            'title' => null,
            'viewBackendPath' => null,
            'viewFrontendPath' => null,
            'routeBackendPath' => null,
            'routeFrontendPath' => null,
        ];
        $this->repositoryInterface = null;
        $this->model = null;
        $this->permissions =
            [
                'index',
                'create',
                'edit',
                'show',
                'destroy',
            ];
        return $this;
    }

    public function __get($name)
    {
        // TODO: fix this validation on ResourceControllerSetUp
//        if (!isset($this->attributes[$name])) {
//            throw new InvalidArgumentException("Invalid attribute call [$name] in ".self::class);
//        }

        if (
            is_null($this->attributes[$name]) &&
            (!is_null($this->model) || !is_null($this->repositoryInterface))
        ) {

            $MODEL = $this->getModel();

            if ($MODEL instanceof ResourceConfigInterface) {
                /** @var ResourceConfigInterface $MODEL */
                switch ($name) {
                    case 'title':
                        return $MODEL::resourceModelName();

                    case 'viewBackendPath':
                        return $MODEL::viewBackendPath();

                    case 'viewFrontendPath':
                        return $MODEL::viewFrontendPath();

                    case 'routeBackendPath':
                        return $MODEL::routeBackendPath();

                    case 'routeFrontendPath':
                        return $MODEL::routeFrontendPath();
                }
            }
        }

        return $this->attributes[$name];
    }

    /**
     * @param  string  $viewPath
     *
     * @return $this
     */
    public function viewBackendPath(string $viewPath): self
    {
        $this->attributes['viewBackendPath'] = $viewPath;
        return $this;
    }

    /**
     * @param  string  $viewPath
     *
     * @return $this
     */
    public function viewFrontendPath(string $viewPath): self
    {
        $this->attributes['viewFrontendPath'] = $viewPath;
        return $this;
    }

    /**
     * @param  string  $title
     *
     * @return $this
     */
    public function title(string $title): self
    {
        $this->attributes['title'] = $title;
        return $this;
    }

    /**
     * @param  string  $routePath
     *
     * @return $this
     */
    public function routeBackendPath(string $routePath): self
    {
        $this->attributes['routeBackendPath'] = $routePath;
        return $this;
    }

    /**
     * @param  string  $routePath
     *
     * @return $this
     */
    public function routeFrontendPath(string $routePath): self
    {
        $this->attributes['routeFrontendPath'] = $routePath;
        return $this;
    }


}
