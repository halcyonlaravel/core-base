<?php

namespace HalcyonLaravelBoilerplate\CoreBase\SetUp;

class DeletedControllerSetUp extends ResourceControllerSetUp
{
    public function reset(): BaseControllerSetUp
    {
        $b = parent::reset();

        $this->permissions =
            [
                'deleted',
                'restore',
                'purge',
            ];

        return $b;
    }

}
