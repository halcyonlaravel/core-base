<?php

namespace HalcyonLaravelBoilerplate\CoreBase\SetUp;

class TableControllerSetUp extends BaseControllerSetUp
{
    public string $index = 'index';

    /**
     * @return \HalcyonLaravelBoilerplate\CoreBase\SetUp\BaseControllerSetUp
     */
    public function reset(): BaseControllerSetUp
    {
        $this->repositoryInterface = null;

        return $this;
    }

    /**
     * @param  string  $index
     *
     * @return $this
     */
    public function index(string $index)
    {
        $this->index = $index;

        return $this;
    }
}

