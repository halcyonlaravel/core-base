<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Transformers;

use Illuminate\Support\Arr;

class MediaTransformer extends BaseTransformer
{

    public function transform($media): array
    {
        /** @var \HalcyonLaravelBoilerplate\MediaLibraryUploader\Models\Media|\HalcyonLaravelBoilerplate\CoreBase\Models\Media $media */

        return [
            'id' => $this->forId($media),
            'type' => $media->type,
            'collection_name' => $media->collection_name,
            'name' => $media->name,
            'file_name' => $media->file_name,
            'mime_type' => $media->mime_type,
            'size' => $media->human_readable_size,
            'order_column' => $media->order_column,
            'custom_properties' => Arr::except($media->custom_properties, 'generated_conversions'),
//            'original_file' => $media->getFullUrl(),
            'generated_conversions' => $media->generatedConversionUrls(),
        ];
    }

    public static function getResourceKey(): string
    {
        return 'media';
    }
}
