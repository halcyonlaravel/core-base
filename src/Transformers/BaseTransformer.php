<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Transformers;

//use App\Transformers\TagTransformer;
use HalcyonLaravelBoilerplate\Money\MoneyFacade as Money;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Date;
use InvalidArgumentException;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;
use Money\Money as PHPMoney;
use Spatie\MediaLibrary\HasMedia;

abstract class BaseTransformer extends TransformerAbstract
{

    public function includeMedia(HasMedia $model): Collection
    {
        return $this->collection(
            $model->media()->orderBy('order_column')->get(),
            MediaTransformer::class
        );
    }

    abstract public static function getResourceKey(): string;

    protected function collection($data, $transformer, ?string $resourceKey = null): Collection
    {
        $transformer = $this->checkTransformer($transformer);
        return parent::collection($data, $transformer, $resourceKey ?: $transformer->getResourceKey());
    }

    protected function item($data, $transformer, ?string $resourceKey = null): Item
    {
        $transformer = $this->checkTransformer($transformer);
        return parent::item($data, $transformer, $resourceKey ?: $transformer->getResourceKey());
    }

//    protected function includeTags(Model $model)
//    {
//        return $this->collection($model->tags, TagTransformer::class);
//    }

    protected static function filterByRoles(array $response, array $data, array $roleNames = null): array
    {
        if (app('auth')->check() && app('auth')->user()->hasAnyRole(
                is_null($roleNames) ? config('core-base-permissions.roles.system') : $roleNames
            )) {
            return array_merge($response, $data);
        }

        return $response;
    }

    protected static function addTimesHumanReadable(
        Model $entity,
        array $responseData,
        array $columns = [],
        bool $isIncludeDefault = true
    ): array {
//        if (!$auth->check()) {
//            return $responseData;
//        }

//        if (!$auth->user()->hasAnyRole(config('core-base-permissions.roles.system'))) {
//            return $responseData;
//        }

        $isHasCustom = count($columns) > 0;

        $defaults = ['created_at', 'updated_at', 'deleted_at'];

        // only custom
        if ($isHasCustom && !$isIncludeDefault) {
            $toBeConvert = $columns;
        }  // custom and defaults
        elseif ($isHasCustom && $isIncludeDefault) {
            $toBeConvert = array_merge($columns, $defaults);
        } // only defaults
        else {
            $toBeConvert = $defaults;
        }

        $return = [];
        foreach ($toBeConvert as $column) {
            $return = array_merge(
                $return,
                (!is_null($entity->{$column})) ? array_merge($return, self::readableTimestamp($column, $entity)) : []
            );
        }

        return array_merge($responseData, $return);
    }

    protected static function readableTimestamp(string $column, $entity): array
    {
        if ($entity instanceof Model) {
            // sometime column is not carbonated, i mean instance if Carbon/Carbon
            $at = Date::parse($entity->{$column});
        } elseif ($entity instanceof Carbon) {
            $at = $entity;
        } elseif (is_string($entity)) {
            $at = Date::parse($entity);
        } else {
            throw new InvalidArgumentException(
                'Invalid $entity argument'
            );
        }

        $tz = $at->timezone(
            app('auth')->guard('api')->user()->timezone
            ?? config('core-base.user_timezone')
            ?? config('app.timezone')
        );

        return [
            $column => $tz->format(config('core-base.formats.datetime_12')),
            $column.'_readable' => $tz->diffForHumans(),
        ];
    }

    protected static function addCustomTimestamp(
        string $name,
        array $responseData,
        Carbon $timestamp
    ): array {
        return array_merge($responseData, self::readableTimestamp($name, $timestamp));
    }

    protected function nullableItem($data, $transformer, $resourceKey = null): ?Item
    {
        if (empty($data)) {
            return null;
        }
        return $this->item($data, $transformer, $resourceKey);
    }

    protected function nullableCollection($data, $transformer, $resourceKey = null): ?Collection
    {
        if (empty($data)) {
            return null;
        }
        return $this->collection($data, $transformer, $resourceKey);
    }

    protected static function moneyTransform(PHPMoney $money): array
    {
        return Money::transform($money);
    }

    /** @deprecated dont fetch data inside tranformers */
    protected function filterEnabledStatusNullableCollection($model, $transformer)
    {
        /** @var \Illuminate\Database\Eloquent\Model|\Spatie\ModelStatus\HasStatuses $model */
        $data = $model->currentStatus(config('core-base.statuses.default.enabled'))->get();
        return $this->nullableCollection($data, $transformer);
    }

    /** @deprecated dont fetch data inside tranformers */
    protected function filterEnabledStatusNullableItem($model, $transformer)
    {
        /** @var \Illuminate\Database\Eloquent\Model|\Spatie\ModelStatus\HasStatuses $model */
        $data = $model->currentStatus(config('core-base.statuses.default.enabled'))->first();
        return $this->nullableItem($data, $transformer);
    }

    protected static function forId(Model $model)
    {
        return $model->{$model->getRouteKeyName()};
    }

    private static function checkTransformer($transformer): self
    {
        if (is_string($transformer)) {
            $transformer = app($transformer);
        }
        return $transformer;
    }

}
