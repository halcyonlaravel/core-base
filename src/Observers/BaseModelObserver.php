<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Observers;

use HalcyonLaravelBoilerplate\MediaLibraryUploader\MediaLibraryUploader;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\HasMedia;

/**
 * @deprecated use verbose logic
 */
abstract class BaseModelObserver
{
    protected Request $request;

    /** @var mixed */
    protected $requestValue = null;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param  \Spatie\MediaLibrary\HasMedia  $model
     * @param  string  $requestKeyName
     * @param  string  $collectionName
     *
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig
     */
    protected function uploadMedia(HasMedia $model, string $requestKeyName = 'image', string $collectionName = 'image')
    {
        if (!$this->isRequestHasExistValue($requestKeyName)) {
            return;
        }
        /** @var \Spatie\MediaLibrary\HasMedia|\HalcyonLaravelBoilerplate\MediaLibraryUploader\Models\Traits\MediaLibraryUploaderTrait $model */
        if ($model->isMediaCollectionMultipleFiles($collectionName)) {
            foreach ($this->requestValue as $image) {
                MediaLibraryUploader::setModel($model)
                    ->file($image)
                    ->execute($collectionName);
            }
        } else {
            MediaLibraryUploader::setModel($model)
                ->file($this->requestValue)
                ->execute($collectionName);
        }
    }

    /**
     * @param  string  $requestKeyName
     *
     * @return bool
     */
    protected function isRequestHasExistValue(string $requestKeyName): bool
    {
        $this->requestValue = null;

        if (
            $this->request->has($requestKeyName) &&
            !blank($value = $this->request->{$requestKeyName})
        ) {
            $this->requestValue = $value;
            return true;
        }

        return false;
    }

    protected function touch($object)
    {
        touch_timestamp_model($object);
    }

    /**
     * @param  \Spatie\ModelStatus\HasStatuses  $model
     * @param  string  $requestKeyName
     *
     * @throws \Spatie\ModelStatus\Exceptions\InvalidStatus
     */
    protected function updateStatus($model, string $requestKeyName = 'status')
    {
        if (!$this->isRequestHasExistValue($requestKeyName)) {
            return;
        }

        $status = $this->requestValue;

        if ($model->status() != $status) {
            $model->setStatus($status);
        }
    }
}
