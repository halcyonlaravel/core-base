<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Facades;

use Carbon\Carbon;
use HalcyonLaravelBoilerplate\CoreBase\Timezone;
use Illuminate\Support\Facades\Facade;

/**
 * @method static Carbon convertToLocal(Carbon $datetime, $userColumn = 'timezone')
 * @method static Carbon convertFromLocal($datetime, $userColumn = 'timezone')
 * @method static format(Carbon $datetime, ?string $format = 'datetime_12', bool $isIncludeTimezone = false)
 */
class TimezoneFacade extends Facade
{
    public static function getFacadeAccessor(): string
    {
        return Timezone::class;
    }
}
