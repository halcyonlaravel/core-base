# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## v8.2.0.alpha.5 - 2023-06-09

### Added

- Nothing.

### Changed

- Mark as deprecated BaseControllerSetUp::repositoryInterface().

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Fix outdated changelog.

## v8.2.0.alpha.4 - 2023-05-05

### Added

- Nothing.

### Changed

- Nothing.

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Fix get model in index in ResourceController.

## v8.2.0.alpha.3 - 2023-05-05

### Added

- Nothing.

### Changed

- Set nullable $repositoryInterface property in BaseControllerSetUp

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Fix nullable $model on ResourceController.

## v8.2.0.alpha.2 - 2023-05-05

### Added

- Nothing.

### Changed

- Set nullable $model property in BaseControllerSetUp

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Nothing.

## v8.2.0.alpha.1 - 2023-05-04

### Added

- Nothing.

### Changed

- Use model instead of repository, and mark repository as deprecated.

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Nothing.

## v8.1.0 - 2023-04-28

### Added

- Nothing.

### Changed

- Make resource controller actionable.

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Nothing.

## 8.0.0 - 2023-03-24

### Added

- Nothing.

### Changed

- Use https://packagist.org/

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Nothing.
