<?php

return [
    'roles' => [
        'system' => 'System',
        'admin' => 'Admin',
        'store_manager' => 'Store Manager',
        'cashier' => 'Cashier',
        'user' => 'User',
    ],
    'permissions' => [
        'view_backend' => 'View Backend',
        'setting_edit' => 'Setting Edit',
        'about_edit' => 'About Edit',
        'contact_us_edit' => 'Contact Us Edit',
        'social_edit' => 'Social Edit',
    ],
];
