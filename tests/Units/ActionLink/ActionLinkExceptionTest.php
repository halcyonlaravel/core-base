<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Test\Units\ActionLink;

use HalcyonLaravelBoilerplate\CoreBase\Exceptions\ActionLinkException;
use HalcyonLaravelBoilerplate\CoreBase\Models\ActionLinks\ActionLink;
use HalcyonLaravelBoilerplate\CoreBase\Models\ActionLinks\Link;

class ActionLinkExceptionTest extends BaseActionLinkTest
{

    /** @test */
    public function side_single()
    {
        $this->expectException(ActionLinkException::class);
        $this->expectExceptionMessage('Argument side `xxx` not found on model action links.');

        $this->generateLinks()->action('xxx', 'show');
    }

    /** @test */
    public function side_multiple()
    {
        $this->expectException(ActionLinkException::class);
        $this->expectExceptionMessage('Argument side `xxx` not found on model action links.');

        $this->generateLinks()->actions('xxx', ['show']);
    }

    /** @test */
    public function type_single()
    {
        $this->expectException(ActionLinkException::class);
        $this->expectExceptionMessage('Argument type `xxx` not found on model action links.');

        $this->generateLinks()->action('backend', 'xxx');
    }

    // no need to test, since this is dynamic
//    public function type_multiple()
//    {
//
//    }

    /** @test */
    public function overRideDefaultKey()
    {
        $this->expectException(ActionLinkException::class);
        $this->expectExceptionMessage('Invalid key `xxx` for overRideDefault in actionLinks.');

        $this->generateLinks(
            ActionLink::create()
                ->add(
                    'backend',
                    Link::make('show', 'http://localhost')->overRideDefault(
                        [
                            'xxx' => '--',
                        ]
                    )
                )
        );
    }

    /** @test */
    public function overRideDefaultIsMore()
    {
        $this->expectException(ActionLinkException::class);
        $this->expectExceptionMessage('Key `is_more` in action link must be bool, `string` given');

        $this->generateLinks(
            ActionLink::create()
                ->add(
                    'backend',
                    Link::make('show', 'http://localhost')->overRideDefault(
                        [
                            'is_more' => 'xxx',
                        ]
                    )
                )
        );
    }

    /** @test */
    public function attribute_multiple()
    {
        $this->expectException(ActionLinkException::class);
        $this->expectExceptionMessage('Attribute `xxx` not found in '.Link::class);

        $this->generateLinks()->actions('backend', ['show'])[0]->xxx;
    }

    /** @test */
    public function attribute_single()
    {
        $this->expectException(ActionLinkException::class);
        $this->expectExceptionMessage('Attribute `xxx` not found in '.Link::class);

        $this->generateLinks()->action('backend', 'show', 'xxx');
    }

    /** @test */
    public function noValue_single()
    {
        $this->expectException(ActionLinkException::class);
        $this->expectExceptionMessage('No value for type `test-type` and attribute `label`');

        $x = $this->generateLinks(
            ActionLink::create()
                ->add(
                    'test-side',
                    Link::make('test-type', 'http://localhost')->overRideDefault(
                        [
                            'label' => null,
                        ]
                    )
                )
        );

        $x->action('test-side', 'test-type', 'label');
    }

    /** @test */
    public function noValue_multiple()
    {
        $this->expectException(ActionLinkException::class);
        $this->expectExceptionMessage('No value for type `test-type` and attribute `label`');

        $x = $this->generateLinks(
            ActionLink::create()
                ->add(
                    'test-side',
                    Link::make('test-type', 'http://localhost')->overRideDefault(
                        [
                            'label' => null,
                        ]
                    )
                )
        );

        $x->actions('test-side', ['test-type'])[0]->label;
    }
}