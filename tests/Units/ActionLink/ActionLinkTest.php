<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Test\Units\ActionLink;

class ActionLinkTest extends BaseActionLinkTest
{

    /** @test */
    public function basic_single_link()
    {
        $this->assertEquals(
            'http://localhost/model/1',
            $this->generateLinks()->action('backend', 'show')
        );
    }

    /** @test */
    public function basic_multiple_link()
    {
        $links = $this->generateLinks()->actions('backend', ['show', 'edit']);

        $actual = [
            $links[0]->url,
            $links[1]->url,
        ];

        $expected = [
            'http://localhost/model/1',
            'http://localhost/model/1/edit',
        ];

        $this->assertCount(2, $links);

        $this->assertEquals(
            $expected,
            $actual
        );
    }
}
