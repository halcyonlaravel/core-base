<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Test\Units\ActionLink;

use HalcyonLaravelBoilerplate\CoreBase\Models\ActionLinks\ActionLink;
use HalcyonLaravelBoilerplate\CoreBase\Models\ActionLinks\Link;
use HalcyonLaravelBoilerplate\CoreBase\Test\Helper\Models\TestModel;

class ActionLinkOverrideDefaultValuesTest extends BaseActionLinkTest
{
    /** @test */
    public function over_ride_default()
    {
        $obj = $this->generateLinks(
            ActionLink::create()
                ->model(new TestModel())
                ->add(
                    'backend',
                    Link::make('show', 'http://localhost')->overRideDefault(
                        [
                            'label' => 'over ride label',
                        ]
                    )
                )
        );


        $showLink = $obj->actions('backend', ['show'])[0];

        $this->assertEquals('over ride label', $showLink->label);
        $this->assertEquals('btn-info', $showLink->class_btn);
    }
}
