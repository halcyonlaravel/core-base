<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Test\Units\ActionLink;

class PermissionTest extends BaseActionLinkTest
{

    /** @test */
    public function user_has_no_any_permissions()
    {
        $this->assertEquals(
            '',
            $this->generateLinks(null, false, $this->testUserNoPermissions)->action('backend', 'show')
        );
    }


    /** @test */
    public function user_has_any_permissions()
    {
        $this->assertEquals(
            'http://localhost/model/1',
            $this->generateLinks(null, false)->action('backend', 'show')
        );
    }


    /** @test */
    public function basic_multiple_link_user_has_any_permissions()
    {
        $links = $this->generateLinks(null, false)->actions('backend', ['show', 'edit']);

        $actual = [
            $links[0]->url,
            $links[1]->url,
        ];

        $expected = [
            'http://localhost/model/1',
            'http://localhost/model/1/edit',
        ];

        $this->assertCount(2, $links);

        $this->assertEquals(
            $expected,
            $actual
        );
    }

    /** @test */
    public function basic_multiple_link_user_has_no_any_permissions()
    {
        $links = $this->generateLinks(null, false, $this->testUserNoPermissions)->actions('backend', ['show', 'edit']);


        $this->assertCount(0, $links);
    }

    /** @test */
    public function get_resource_on_valid_permissions()
    {
        /** @var \HalcyonLaravelBoilerplate\CoreBase\Test\Helper\Models\TestModel $model */
        $model = $this->generateLinks(null, false);

        $resources = $model->actions('backend', ['show', 'edit', 'destroy']);

        $this->assertCount(3, $resources);
        $this->assertCount(2, $model->actions('backend', ['restore', 'purge']));
    }

}