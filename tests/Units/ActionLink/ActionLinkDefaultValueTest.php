<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Test\Units\ActionLink;

class ActionLinkDefaultValueTest extends BaseActionLinkTest
{
    /** @test */
    public function get_default_value_show()
    {
        $link = $this->generateLinks()->actions('backend', ['show'])[0];

        $this->assertEquals('Show', $link->label);
        $this->assertEquals('btn-info', $link->class_btn);
        $this->assertEquals('btn_show', $link->name);
        $this->assertEquals('fa fa-eye', $link->icon);
    }

}
