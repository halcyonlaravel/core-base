<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Test\Units\ActionLink;

use HalcyonLaravelBoilerplate\CoreBase\Models\ActionLinks\ActionLink;
use HalcyonLaravelBoilerplate\CoreBase\Models\Traits\ActionLinkTrait;
use HalcyonLaravelBoilerplate\CoreBase\Test\Helper\Models\TestModel;
use HalcyonLaravelBoilerplate\CoreBase\Test\Helper\Models\User;
use HalcyonLaravelBoilerplate\CoreBase\Test\TestCase;
use Illuminate\Support\Facades\Gate;

abstract class BaseActionLinkTest extends TestCase
{

    /**
     * @param  \HalcyonLaravelBoilerplate\CoreBase\Models\ActionLinks\ActionLink|null  $actionLinks
     *
     * @param  bool  $applyGateAlwaysTrue
     *
     * @param  \HalcyonLaravelBoilerplate\CoreBase\Test\Helper\Models\User|null  $user
     *
     * @return ActionLinkTrait
     */
    protected function generateLinks(
        ?ActionLink $actionLinks = null,
        bool $applyGateAlwaysTrue = true,
        User $user = null
    ) {
        $this->actingAs($user ?: User::first());

//        $this->mock(GateContract::class, function ($mock) {
//
//            $mock->shouldReceive('can')->once()->andReturn(true);
//        });
        if ($applyGateAlwaysTrue) {
            Gate::after(fn() => true);
        }
//        else {
//
//            Gate::shouldReceive('authorize')
////            ->once()
////                ->with($permissionName)
//                ->andReturn(true);
//        }
//Gate::authorize()


        if (is_null($actionLinks)) {
            return new TestModel();
        }

        return new class($actionLinks) {
            use ActionLinkTrait;

            private $actionLinks;

            public function __construct(?ActionLink $actionLinks)
            {
                $this->actionLinks = $actionLinks;
            }

            public function actionLinks(): ActionLink
            {
                return $this->actionLinks;
            }
        };
    }
}
