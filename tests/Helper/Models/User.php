<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Test\Helper\Models;

use Spatie\Permission\Traits\HasRoles;

class User extends \Illuminate\Foundation\Auth\User
{
    use HasRoles;

    public string $guard_name = 'web';

    public $timestamps = false;


    protected $fillable = [
        'email',
    ];
}