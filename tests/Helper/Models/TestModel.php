<?php

namespace HalcyonLaravelBoilerplate\CoreBase\Test\Helper\Models;

use HalcyonLaravelBoilerplate\CoreBase\Models\ActionLinks\ActionLink;
use HalcyonLaravelBoilerplate\CoreBase\Models\ActionLinks\Link;
use HalcyonLaravelBoilerplate\CoreBase\Models\Contracts\PermissionInterface;
use HalcyonLaravelBoilerplate\CoreBase\Models\Contracts\ResourceConfigInterface;
use HalcyonLaravelBoilerplate\CoreBase\Models\Traits\ActionLinkTrait;
use HalcyonLaravelBoilerplate\CoreBase\Models\Traits\GenerateResourceConfigTrait;
use Illuminate\Database\Eloquent\Model;

class TestModel extends Model implements ResourceConfigInterface, PermissionInterface
{
    use ActionLinkTrait;
    use GenerateResourceConfigTrait;

    public static function icon(): string
    {
        return '';
    }

    public static function accessPermissions(): array
    {
        return [
            // resources
            'index' => 'test model list',
            'create' => 'test model create',
            'edit' => 'test model edit',
            'show' => 'test model show',
            'destroy' => 'test model destroy',

            // deletes
            'deleted' => 'test model deleted list',
            'restore' => 'test model restore',
            'purge' => 'test model purge',

        ];
    }

    public function actionLinks(): ActionLink
    {
        return ActionLink::create()
            ->model($this)
            ->add('backend', Link::make('show', 'http://localhost/model/1'))
            ->add('backend', Link::make('edit', 'http://localhost/model/1/edit'))
            ->add('backend', Link::make('destroy', 'http://localhost/model/1/destroy'))
            ->add('backend', Link::make('restore', 'http://localhost/model/1/restore'))
            ->add('backend', Link::make('purge', 'http://localhost/model/1/purge'));
    }
}